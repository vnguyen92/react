import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import registerServiceWorker from './registerServiceWorker';
import FormExample from './App';
// not render 2 components together into 1 root 

// function Welcome(props) {
//     return <h1>Hello, {props.name}</h1>;
// }

// const element = <Welcome name="Sara" />;
// ReactDOM.render(
//     element,
//     document.getElementById('root')
// );
ReactDOM.render(<App />, document.getElementById('root'));
var form = new FormExample();
form.showHardData();
registerServiceWorker();
