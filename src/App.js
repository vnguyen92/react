import React from 'react';
import './App.css';
import { Button, Row, Col, Grid, FormGroup, ControlLabel, FormControl, HelpBlock, Table } from 'react-bootstrap';
import ReactDOM from 'react-dom';
import $ from 'jquery';
function FieldGroup({ id, label, help, ...props }) {
  return (
    <FormGroup controlId={id}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} />
      {help && <HelpBlock>{help}</HelpBlock>}
    </FormGroup>
  );
}
function CreateRowData(serial, firstname, lastname, email, mobilenumber, role, department) {
  return <tr>
    <td> {serial}</td>
    <td> {firstname} {lastname} </td>
    <td>{email}</td>
    <td>{mobilenumber}</td>
    <td>{role}</td>
    <td>{department}</td>
    <td>
      <Button className="btn-glyphicon" bsSize="small"><span className="glyphicon glyphicon-edit">Edit</span> </Button>
      <Button className="btn-glyphicon" bsSize="small"><span className="glyphicon glyphicon-trash">Remove</span> </Button>
    </td>
  </tr>
}
class FormExample extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      harddata: [{
        id: 456789,
        "firstname": "Vinh",
        "lastname": "Nguyen",
        "email": "vnguyen92@dxc.com",
        "mobilenumber": "0911901825",
        "role": "Front End Developer",
        "department": "",
        "companyname": "DXC Vietnam",
        "researchfrequency": "Vinh"
      },
      {
        id: 456123,
        "firstname": "Trang",
        "lastname": "Nguyen",
        "email": "tnguyen12@dxc.com",
        "mobilenumber": "0911901825",
        "role": "Front End Developer",
        "department": "",
        "companyname": "DXC Vietnam",
        "researchfrequency": "TRANG"
      }, {
        id: 456234,
        "firstname": "Huy",
        "lastname": "Dong",
        "email": "hdong5@dxc.com",
        "mobilenumber": "0911901825",
        "role": "Front End Developer",
        "department": "",
        "companyname": "DXC Vietnam",
        "researchfrequency": "HUY"
      },
      {
        id: 456345,
        "firstname": "Tai",
        "lastname": "Le",
        "email": "tle62@dxc.com",
        "mobilenumber": "0911901825",
        "role": "Front End Developer",
        "department": "",
        "companyname": "DXC Vietnam",
        "researchfrequency": "TAI"
      },],

      "newData":
      {
        id: Math.round(10000 * Math.random()),
        "firstname": "",
        "lastname": "",
        "email": "",
        "mobilenumber": "",
        "role": "",
        "department": "",
        "companyname": "",
        "researchfrequency": ""
      },

    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }
  addData(data, index) {
    var firstname = data.firstname;
    var lastname = data.lastname;
    var email = data.email;
    var mobilenumber = data.mobilenumber;
    var role = data.role;
    var department = data.department;
    return CreateRowData(index, firstname, lastname, email, mobilenumber, role, department);
  }
  showHardData() {
    var form = new FormExample();
    var listOfRow = [];
    for (var i = 0; i < form.state.harddata.length; i++) {
      var data = form.state.harddata[i];
      var rowData = this.addData(data, i + 1);
      listOfRow.push(rowData);
    }
    var tbody = document.getElementsByTagName("tbody");
    ReactDOM.render(listOfRow, tbody[0]);
  }
  handleChange(event) {
    var name = event.target.name;
    var value = event.target.value;
    var obj = this.state.newData;
    this.setState({
      "newData":
      {
        "id": obj.id,
        "firstname": obj.firstname,
        "lastname": obj.lastname,
        "email": obj.email,
        "mobilenumber": obj.mobilenumber,
        "role": obj.role,
        "department": obj.department,
        "companyname": obj.companyname,
        "researchfrequency": obj.researchfrequency,
        [name]: value
      }
    });
  }
  submitForm() {
    console.log(">>>>>>>>3", this.state.newData);
    var tbody = document.getElementsByTagName("tbody");
    ReactDOM.render(this.addData(this.state.newData, 0), tbody[0]);

  }

  render() {
    return (
      <div className="container-fluid">
        <div className="form-info">
          <i className="fa fa-info-circle"></i>
          <h4>Create Information</h4>
        </div>
        <div className="container" >
          <form className="form-container">
            <Grid>
              <Row className="show-grid">
                <Col xs={6} md={6}>
                  <FieldGroup
                    id="formControlsText"
                    type="text"
                    label="FirstName(*)"
                    placeholder="Enter First Name"
                    name="firstname"
                    help="dasdasdas"
                    onChange={this.handleChange}
                  />
                  <FieldGroup
                    id="formControlsEmail"
                    type="email"
                    label="Email address(*)"
                    placeholder="Enter email"
                    name="email"
                    onChange={this.handleChange}
                  />
                  <FormGroup controlId="formControlsSelect">
                    <ControlLabel>Role(*)</ControlLabel>
                    <FormControl
                      name="role"
                      onChange={this.handleChange} componentClass="select" placeholder="Role">
                      <option disabled selected="selected" hidden>Select your role </option>
                      <option defaultValue="Front End Developer">Front End Developer</option>
                      <option defaultValue="Software Engineer">Software Engineer</option>
                      <option defaultValue="Computer Science">Computer Science</option>
                      <option defaultValue="BA">BA</option>
                    </FormControl>
                  </FormGroup>
                  <FieldGroup
                    id="formControlsText"
                    type="text"
                    label="Company Name"
                    placeholder="Company name"
                    name="companyname"
                    onChange={this.handleChange}
                  />
                  <Button className="btn-cancel">CANCEL</Button>
                </Col>
                <Col xs={6} md={6}>
                  <FieldGroup
                    id="formControlsText"
                    type="text"
                    label="Last name(*)"
                    name="lastname"
                    onChange={this.handleChange}
                    placeholder="Enter last name"

                  />
                  <FieldGroup
                    id="formControlsText"
                    type="text"
                    label="Mobile Number(*)"
                    placeholder="Enter Mobile Number"
                    name="mobilenumber"
                    onChange={this.handleChange}
                  />

                  <FieldGroup
                    id="formControlsText"
                    type="text"
                    label="Department"
                    name="department"
                    onChange={this.handleChange}
                    placeholder="Enter Department"
                  />
                  <FieldGroup
                    id="formControlsText"
                    type="text"
                    label="Research Frequency"
                    name="researchfrequency"
                    onChange={this.handleChange}
                    placeholder="Enter Research Frequency"
                  />
                  <Button bsStyle="primary" onClick={this.submitForm}>SUBMIT</Button>
                </Col>
              </Row>
            </Grid>
          </form>
          <Table responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>Full name</th>
                <th>Email</th>
                <th>Mobile number</th>
                <th>Department</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </Table>
        </div >
      </div>


    );
  }
}

export default FormExample;
